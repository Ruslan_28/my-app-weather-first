import React, { Component } from 'react';
import './App.css';
import '../node_modules/font-awesome/css/font-awesome.min.css';

import SearchContainer from './containers/SearchContainer';
import CityListContainer from './containers/CityListContainer';
import WeatherList from './components/WeatherList'

class App extends Component {
  render() {
    return (
      <main>
        <SearchContainer />
        <CityListContainer />
        <WeatherList />
      </main>
    );
  }
} 

export default App;
