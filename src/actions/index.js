import axios from 'axios';
import URL from '../URL';

export const GET_CITY_LIST = 'GET_CITY_LIST';
export const SEARCH_LIST = 'SEARCH_LIST';
export const ADD_WEATHER_LIST = 'ADD_WEATHER_LIST';

export function getCityList() {
    return dispatch => {

        return axios.get(URL)
        .then(res => res.data)
        .then(cities => dispatch({
            type: GET_CITY_LIST,
            cities
        }))
    }
}

export function searchList(find) {
    return {
        type: SEARCH_LIST,
        find
    }
}

export function addWeatherList(id) {
    return {
        type: ADD_WEATHER_LIST,
        id
    }
}