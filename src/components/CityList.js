import React from 'react';

import Plyha from './Plyha';

class CityList extends React.Component {
    componentWillMount() {
        this.props.getCityList();
    }

    onClick = id => {
        this.props.addWeatherList(id)
        console.log('addWeatherList', id)   
    }

    render() {
        if(!this.props.cities) {
            return <Plyha />
        }
        
        return (
            <div className="CityList">
                {
                    this.props.cities.map(item => {
                        return (
                            <div className="cityItem" key={item.id}>
                                {item.name}
                                <i className="fa fa-plus" onClick={() => this.onClick(item.id)}></i>
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}

export default CityList;