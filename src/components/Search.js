import React from 'react';

import Inputs from './Input';

class Search extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value: ''
        }
    }

    onChange = (e) => {
        this.setState({
            value: e.target.value
        })

        this.props.onSearch(e.target.value)
    }

    render() {
        return (
            <Inputs 
                className="searchCity" 
                placeholder="Search City" 
                type="text"
                value={this.state.value}
                onChange={this.onChange}
            />
        )
    }
}

export default Search;