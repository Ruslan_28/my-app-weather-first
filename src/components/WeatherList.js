import React from 'react';
import { connect } from 'react-redux';

const WeatherList = ({cities}) => (
    <div className="weatherList">
        {cities.map(item => (
            <div key={item.id}>
                <span>{item.name}</span>
            </div>
        ))}
    </div>
)



export default connect(
    state => ({
        cities: state.cities.selectedCities
    })
)(WeatherList);