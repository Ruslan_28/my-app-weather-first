import { connect } from 'react-redux';

import { getCityList } from '../actions';
import { addWeatherList } from '../actions';

import CityList from '../components/CityList';

function mapStateToProps(state) {
    return {
        cities: state.cities.filteredData
    }
}

const CityListContainer = connect(mapStateToProps, { getCityList,  addWeatherList})(CityList);

export default CityListContainer;