import { connect } from 'react-redux';

import { searchList } from '../actions';
import Search from '../components/Search';

function mapDispatchToProps(dispatch) {
    return {
        onSearch: find => dispatch(searchList(find))
    }
}

const SearchContainer = connect(null, mapDispatchToProps)(Search);

export default SearchContainer;