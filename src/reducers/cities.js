import { GET_CITY_LIST } from '../actions';
import { SEARCH_LIST } from '../actions';
import { ADD_WEATHER_LIST } from '../actions';

const initialState = {
    data: null,
    selectedCities: [],
}

function cities(state=initialState, action) {
    switch(action.type) {
        case GET_CITY_LIST:
            return {
                ...state,
                data: action.cities,
            }

        case SEARCH_LIST:
            let filteredData;
            if(action.find) {
                filteredData = state.data.filter(name => name.name.toLowerCase().includes(action.find.toLowerCase()));
            }
            return {
                ...state,
                filteredData,
            }
        
        case ADD_WEATHER_LIST:
            const selectedCities = state.selectedCities.concat();

            const { id } = action;
            
            const pushCity = state.data.find(item => item.id === id);

            selectedCities.push(pushCity)
            
            return {
                ...state,
                selectedCities
            }
        
        default:
            return state;
    }
}

export default cities;